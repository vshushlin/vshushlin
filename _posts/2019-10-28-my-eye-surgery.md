---
layout: single
title:  "My eye surgery"
date:   2019-10-28 12:12:00 +0800
excerpt_separator: <!--more-->
---

### Note

_This post is not about my experience with the surgery itself.
There are already a lot of articles about that.
Here I want to describe how I decided to do it,
or even what people and events influenced my actions._
<!--more-->

### A little background

I had been having a bad vision for a long time.
I was wearing my glasses all the time for the last ten years.
If it tells you anything, my vision was about -5D and -1.5D Cyl for both eyes.

And at some point, you'll start thinking about solving this problem once and for all.
I can't recall when I first start seriously thinking the surgery,
guess, it was some kind of constant buzz around consisting of people mentioning this topic
now and then.

### Part I: Joking around

About 2 years ago I was going to the office after lunch with a friend of mine.
The dialog went something like this:
> Me: One day I'll just fix my eyes with the laser.
>
> My friend: If you'll do it, do the SMILE surgery. If I recall correctly this is the best technology at the moment.

_Remember the "SMILE" acronym, it's gonna change a few times._
It is also worth mentioning that I had a very good reason to trust my friend's judgment in this area.

We used to have _beer and counter-strike nights_ on Fridays in the sales and support department.
So on the same week Friday night I had this discussion with one of our top salespeople:

> ...
>
> me: Yeah, I want to do eye surgery.
>
> SP: That's cool, my friend did it in Kazan a while ago and she's super happy about it!
>
> me: I'm not sure...
>
> SP: Why? This is the best way to do it! It's twice as cheap compared to Moscow!
> And they use Femto Lasic - the most advanced technology!
>
> me: Ok, I'll look into it.
>
> SP: Don't look, just do. Believe me, it is the best option!

This salesperson had huge authority _within the tribe_
and I intuitively trusted him.
_So the "the best technology" shifted to "Femto Lasic"._

But the problem is **salespeople are so f\*\*\*ing overconfident**!
They simply assume that they are always right regardless of how little
information they have.
I think that the basic skill of doubting your beliefs prevents people
from being successful in that field, so never trust them even if you're
absolutely sure that they have the best intentions in mind.

### <a name="seeingtheexample"></a>Part II: Seeing someone else doing it

Probably the best motivation to do something comes from observing
how someone else successfully does it.

I changed a job, and one of my new colleagues just took some time off.
When she returned she was wearing sunglasses and bragging about
how awesome is it to see the world with her own eyes.
And she also was trolling other people including me for having to wear glasses.

Her experience sounded incredible: fast, with no pain at all, with relatively easy restrictions.
The only described problems were:
* 2 weeks of not being able to work with the computer;
* half a year of restricted physical activity.

I guess that was the point when the question "Should I do it?" transformed into something like
"Ok, when should I do it?"

### Part III: Some guiding principles

Here are some ideas influenced my execution:
1. I'm never more than 80% confident in any decision or plan and
always leave some room for the last-minute change of plans.
I even give myself an imaginary cookie every time I change an opinion on something or a plan!
1. I try to avoid long planning or even planning at all.
Usually, I just figure out the next obvious action and try to execute it ASAP.
1. If you're doing any kind of surgery, do it in the place which does thousands of them every year.
The experience of the people is much more important than any status signs.
This simple idea was taken from the book [Decisive] which I highly recommend!
1. If I do someone new, the best start will be to ask someone who already done it.
And this one is also from the [Decisive] book.
It sounds obvious, but it's good to push this principle to the limit:
don't even try to solve problem yourself, just ask people you know, and ask more than you feel necessary.

### Part IV: The last kick in the ass

So I had the surgery on my _project list_ in the Todoist.
There was a fall and this is probably the best to do it.
But there were a lot of other things to do
and this one has never had a high enough priority.
It also wasn't broken down into anything actionable,
which is the best way to never get it done.

There is [this interesting game called "Zareshai"](https://prapion.me/zareshai).
I wanted to try it for a while, so eventually, I came around to it.
I guess [this article on comfort zone expansion](https://www.lesswrong.com/posts/c5wFM7KJLtuMnLFsH/hammertime-day-5-comfort-zone-expansion) triggered me.
I originally had chosen a bad problem to work with - basically, it wasn't a problem at all.
And near to the of the game, I switched to "do eye surgery".
I only finished the goal formulation part of the game, but It was somewhat helpful.

In any case, the main outcome of this was that I finally made a decision to do it,
and do it fast.

### Part V: a quick start

The usual way for me to kick something of is to use [Yoda timers].
I set a timer for 5 minutes and tried "to solve the problem in these 5 minutes".
The most obvious thing to do, and probably one of only possible in 5 minutes,
is to ask people.
And one of the reasons I like Yoda timers so much is that they push me to write
to people right now!
I'm sort of an introvert and initiation of dialogue is always a very good candidate to postpone.

So I set a timer and wrote to a few people including my sister who I knew was planning to fix her eyes
a while ago.
It turned out that she did the surgery a half year ago and was super happy about the result.
So I just asked the name of the clinic and one yoda-timer later I filled the callback form on their
website.
They called 10 minutes later despite it's being Sunday, and I went for a checkup the very next day.

### Part VI: the checkup itself

So I went to a clinic and the experience was quite nice there.
It was a private business so everyone was super nice to me.
At the end the doctor said:

> Yes, nothing prevents you from doing surgery.
Here is the price list for the different technologies we have.
I'd recommend you to do the best technology which is "Femto Lasic", but it's twice as expensive.

So everything was settled, I just needed to pick the date.
The only thing preventing me from doing this the very next week was the appointments schedule
which had a conflict with my travel plans.

_Note: at this point, the decision was to do the surgery at earliest convenience at this private clinic and do Femto Lasic._

### Part VII: social approval

We have this question at daily standups: "What is something awesome did you do yesterday outside of work?".
And I mentioned that I went for a checkup.
And there were **a lot of positive and encouraging feedback!**
Most of it was along the lines of "I did it a while ago and it was the best decision of my life!".

### 20% possibility of changing everything at the very last moment

Changing plans and opinions especially after I publicly declared them is hard,
it's probably a few times easier for me than for most of the people,
but sometimes it still requires some willpower.
Especially in cases when I need to throw away the current plan, not for the better plan,
but for clear uncertainty when I don't know what to do and need to figure something
out from scratch all over again.

One of my friends at the bar just mentioned that SMILE was the best technology as far as he remembered
and that is what he would be looking for if he would be doing something.

The next day I googled a bit more on the topic.
SMILE indeed was newer and more widely recommended.

### Don't trust your first impression

So, the [Yoda timer] once again - forcing myself to deal with the problem on the short time frame is such
a wonderful tool!
I simply don't have time to hesitate in these 5 minutes.
Remember the [the colleague example](#seeingtheexample) which convinced me to seriously consider the surgery?
I remembered the name of the clinic she used.
So I just found their website, apparently they were doing SMILE as well, and I went for the checkup.

The clinic was far away and what's worse it was public, so no one cared much about clients.
I also went there close to the end of the day, so they just said that could start the checkup this day
but I wouldn't be able to finish it and need to come the next day anyway.
So I was super frustrated, it also worth mentioning that I hungry.
The first idea was to forget about this clinic and go to the first one.

But I thought that I might be just hungry and frustration with 3 hours lost commuting.
And these two things had absolutely nothing to do with the quality of the clinic itself.

### The end of the story

Next day I went to the checkup, the doctor just said that I should come the next Wednesday
not giving me any options.
They had no mandatory appointments except one on the next day after the surgery.
So I just got some time off at work and went for the surgery.
It's so nice when you can ask for time off only a week in advance.

### And the bottom line

[HPMOR](http://www.hpmor.com/chapter/78):
> Later, looking backward, Harry would think of how, in his SF and fantasy novels, people always made their big, important choices for big, important reasons. Hari Seldon had created his Foundation to rebuild the ashes of the Galactic Empire, not because he would look more important if he could be in charge of his own research group. Raistlin Majere had severed ties with his brother because he wanted to become a god, not because he was incompetent at personal relationships and unwilling to ask for advice on how to do better. Frodo Baggins had taken the Ring because he was a hero who wanted to save Middle-Earth, not because it would've been too awkward not to. If anyone ever wrote a true history of the world - not that anyone ever could or would - probably 97% of all the key moments of Fate would turn out to be constructed of lies and tissue paper and trivial little thoughts that somebody could've just as easily thought differently.

Who, the hell, is controlling my life?
Everything above is just a coincidence, except for very quick moments of me trying awkwardly to take
some control over the direction I heading to.

There is this very cool book called [Mindware],
and one of the most fascinating insights I got from it is that **people are terrible at explaining their own actions**.

And now I can't stop thinking about it and noticing that things just happen around and I mindlessly react to them.

How often do I make my big life decisions based on a single example of the friend,
a [podcast episode](http://www.thebayesianconspiracy.com/2016/06/episode-9-polyamory/)
or temporary mental state, e.g. quitting the job because I was depressed at University?

[Decisive]: https://www.amazon.com/Decisive-Make-Better-Choices-Life/dp/0307956393
[Yoda timers]: https://www.lesswrong.com/s/qRxTKm7DAftSuTGvj/p/vpvKEj7shuk8h5Eet
[Mindware]: https://www.amazon.com/Mindware-Tools-Thinking-Richard-Nisbett/dp/1511357193
