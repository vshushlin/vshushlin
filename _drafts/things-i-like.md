---
layout: single
title:  "Things I like"
date:   2019-10-28 12:12:00 +0800
excerpt_separator: <!--more-->
---
This is just a random collection of stuff I like.
I will update if from time to time and maybe will split it in the future
if it gets too big.

_This is a draft, I'll add my notes for most of items later_

## Podcasts

All of them most likely can be found in your favorite podcast app.

### The Methods of Rationality Podcasts

This is the best audio version of anything I've ever heard! :heart:
Seriously, these people did an awesome job narrating HPMOR book and many other.

I also greatly enjoyed the [Crystal Society audio version](http://www.hpmorpodcast.com/?page_id=1958),
which you can find in later episodes.

* [official website](http://www.hpmorpodcast.com/)

### Slate Star Codex Podcast

* [official website](https://sscpodcast.libsyn.com/)

I wanted to add a list of my favorite episodes, but there are too many of them.
Just search for any "Book Review" or "Much more than you wanted to know".

But here are a few:
* [Book Review: The Black Swan](https://sscpodcast.libsyn.com/book-review-the-black-swan)
* [Melatonin: Much More Than You Wanted to Know](https://sscpodcast.libsyn.com/melatonin-much-more-than-you-wanted-to-know)
* [SSC JOURNAL CLUB SEROTONIN RECEPTORS](https://sscpodcast.libsyn.com/ssc-journal-club-serotonin-receptors)

### The Bayesian Conspiracy

* [official website](http://www.thebayesianconspiracy.com/)

A few episodes:

* [44 – Burning Man](http://www.thebayesianconspiracy.com/2017/09/44-burning-man/)
* [6 – Cryonics](http://www.thebayesianconspiracy.com/2016/04/6-cryonics/)
* [9 – Polyamory (part 1)](http://www.thebayesianconspiracy.com/2016/06/episode-9-polyamory/)s

### Блаблафилия

Sorry, that one is in Russian, but it's awesome!

* [official website](http://apfor.ru/)
* [telegram channel](https://t.me/blablafilia]
* [sample episode for you to try](https://www.youtube.com/watch?v=-odzT9LRgXY&list=PLGFjbKB9uLiktfVhloYREob1oRosxIn9D&index=10)

### Feeling Good Podcast



## Books

To be done...
